# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('valet', '0004_whatsapprequest_ded_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='whatsapprequest',
            name='request',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='whatsapprequest',
            name='ded_type',
            field=models.CharField(default=b'NON', help_text=b'The dedicated landing page the request came from', max_length=3, verbose_name=b'Dedicated LP Type', choices=[(b'NON', b'None'), (b'SCF', b'School Fees'), (b'SHA', b'Ship Abroad'), (b'CNG', b'Expat Concierge')]),
            preserve_default=True,
        ),
    ]
