# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('valet', '0003_auto_20160413_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='whatsapprequest',
            name='ded_type',
            field=models.CharField(default=b'NON', help_text=b'The dedicated landing page the request came from', max_length=3, verbose_name=b'Dedicated LP Type', choices=[(b'NON', b'None'), (b'SCF', b'School Fees')]),
            preserve_default=True,
        ),
    ]
