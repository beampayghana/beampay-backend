from django.contrib import admin

from valet.models import WhatsappRequest


class WhatsappRequestAdmin(admin.ModelAdmin):
    readonly_fields = ['wap_number', 'country', 'created_at']
    list_display = ['wap_number', 'id', 'country', 'created_at', 'handler']


admin.site.register(WhatsappRequest, WhatsappRequestAdmin)
