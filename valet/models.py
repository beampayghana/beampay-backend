from django.db import models

from valet import constants


class WhatsappRequest(models.Model):

    HANDLER_OPTIONS = (
        (constants.NO_ONE, 'No one'),
        (constants.KINGSTON, 'Kingston'),
        (constants.FALK, 'Falk'),
        (constants.GERALD, 'Gerald'),
    )

    DEDICATED_TYPE = (
        (constants.NONE, 'None'),
        (constants.SCHOOL_FEES, 'School Fees'),
        (constants.SHIP_ABROAD, 'Ship Abroad'),
        (constants.CONCIERGE, 'Expat Concierge'),
    )

    wap_number = models.CharField(
        'WhatsApp Number',
        max_length=23,
        help_text='WhatsApp number sent as a request'
    )

    handler = models.CharField(
        'Handler',
        max_length=1,
        help_text='Beam Crew member handling the request',
        default=constants.NO_ONE,
        choices=HANDLER_OPTIONS
    )

    country = models.CharField(
        max_length=100,
        default='None'
    )

    ded_type = models.CharField(
        'Dedicated LP Type',
        help_text='The dedicated landing page the request came from',
        max_length=3,
        default=constants.NONE,
        choices=DEDICATED_TYPE
    )

    request = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '{} - {}'.format(self.wap_number, self.get_handler_display())
