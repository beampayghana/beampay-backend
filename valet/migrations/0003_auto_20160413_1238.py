# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('valet', '0002_auto_20151013_0834'),
    ]

    operations = [
        migrations.AddField(
            model_name='whatsapprequest',
            name='country',
            field=models.CharField(default=b'None', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='whatsapprequest',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 13, 12, 38, 50, 682146, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
